﻿using UnityEngine;
using System.Collections;

public class ControladorPersonaje : MonoBehaviour {

	public float fuerzaSalto = 100f;

	private bool enSuelo = true;
	public Transform comprovadorSuelo;
	private float comprovadorRadio = 0.07f;
	public LayerMask mascaraSuelo;

	private bool dobleSalto = false;

	private Animator animator;

	private bool corriendo = false;
	public float velocidad =1f;

	void Awake(){
		animator = GetComponent<Animator> ();

	}

	void Start () {


	}

	void FixedUpdate(){
		if (corriendo) {
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (velocidad, GetComponent<Rigidbody2D> ().velocity.y);


		}
		animator.SetFloat ("VelX", GetComponent<Rigidbody2D> ().velocity.x);
		enSuelo = Physics2D.OverlapCircle (comprovadorSuelo.position, comprovadorRadio, mascaraSuelo);
		animator.SetBool ("isGrounded", enSuelo);
		if (enSuelo) {
			dobleSalto = false;
		}
	}
		

	void Update () {
		if(Input.GetMouseButtonDown(0)){
			if(corriendo){
			//hecemos sq salte si puede
				if (enSuelo || !dobleSalto) {
					GetComponent<AudioSource>().Play ();
					GetComponent<Rigidbody2D> ().velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, fuerzaSalto);
					//GetComponent<Rigidbody2D>().AddForce (new Vector2 (0, fuerzaSalto));
					if (!dobleSalto && !enSuelo) {
						dobleSalto = true;

					}
				}

			}else{
				corriendo = true;
				NotificationCenter.DefaultCenter ().PostNotification (this, "PersonajeEmpiezaACorrer");
			  }
			}
		}
	}




