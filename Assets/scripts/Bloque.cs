﻿using UnityEngine;
using System.Collections;

public class Bloque : MonoBehaviour {
	private bool haColisionadoConElJugador = false;
	public int puntosGanados= 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter2D(Collision2D collision){
		
		if (!haColisionadoConElJugador && (collision.collider.gameObject.tag == "Finish" || collision.collider.gameObject.tag == "Finish")) {
			NotificationCenter.DefaultCenter().PostNotification(this, "IncrementarPuntos", 1);
			haColisionadoConElJugador = true;
		}
	}




}

